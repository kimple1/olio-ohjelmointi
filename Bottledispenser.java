package com.example.v8;

import java.util.ArrayList;

import java.util.ArrayList;

public class BottleDispenser {
    private int bottles;
    private ArrayList<Bottle> bottle_array;
    private double money;
    private static BottleDispenser bd = null;

    public BottleDispenser() {
        bottles = 5;
        money = 0;

        bottle_array = new ArrayList<Bottle>();
        String n, m;
        double e, s, p;

        for(int i = 0; i<bottles; i++) {
            if(i<2) {
                n = "Pepsi Max";
                m = "Pepsi";
                e = 0.3;
                if(i<1) {
                    s = 0.5;
                    p = 1.80;
                }
                else {
                    s = 1.5;
                    p = 2.20;
                }
            }
            else if(i < 4) {
                n = "Coca-Cola Zero";
                m = "Coca-Cola";
                e = 0.4;
                if(i<3) {
                    s = 0.5;
                    p = 2.00;
                }
                else {
                    s = 1.5;
                    p = 2.50;
                }
            }
            else {
                n = "Fanta Zero";
                m = "Coca-Cola";
                e = 0.5;
                s = 0.5;
                p = 1.95;
            }
            bottle_array.add(i, new Bottle(n, m, e, s, p));
        }
    }

    public static BottleDispenser getInstance(){
        if (bd == null){
            bd = new BottleDispenser();
        }
        return bd;
    }


    public String buyBottle(int b_index) {
        try {
            Bottle b = bottle_array.get(b_index);
            String c = "";
            if(bottles <= 0) {
                c = "No more bottles!";
                return c;
            }
            else if(money < b.getPrize()) {
                c = "Add money first!";
                return c;
            }
            else {
                bottles -= 1;
                money -= b.getPrize();
                c = "KACHUNK! " + b.getName() + " came out of the dispenser!";
                deleteBottle(b);
                return c;
            }
        }
        catch (Exception e) {
            return "Not in inventory";
        }

    }

    public void listBottles() {
        for(int i = 0; i < bottles; i++) {
            System.out.println(i+1 + ". Name: " + bottle_array.get(i).getName());
            System.out.print("\tSize: " + bottle_array.get(i).getSize());
            System.out.println("\tPrice: " + bottle_array.get(i).getPrize());
        }
    }

    private void deleteBottle(Bottle b) {
        bottle_array.remove(b);
    }

    public String returnMoney() {
        String c;
        money = Math.round(money);
        c = "Klink klink. Money came out! You got "+money+"€ back.\n";
        money = 0;
        return c;
    }

    public void addMoney(int amount) {
        money += amount;
    }
}