package com.example.v8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    TextView text;
    SeekBar seekBar;
    TextView textMoney;
    int amount;
    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar  = findViewById(R.id.seekBar);

        text = findViewById(R.id.textView);
        textMoney = findViewById(R.id.textView2);
        text.setText("Soda Machine!");

        Spinner bottlespinner = (Spinner) findViewById(R.id.bottlespinner);
        bottlespinner.setOnItemSelectedListener(this);

        List<String> bottleList = new ArrayList<String>();
        bottleList.add("Pepsi Max 0.5 l");
        bottleList.add("Pepsi Max 1.5 l");
        bottleList.add("Coca-Cola Zero 0.5 l");
        bottleList.add("Coca-Cola Zero 1.5 l");
        bottleList.add("Fanta 0.5 l");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bottleList);
        adapter.setDropDownViewResource((android.R.layout.simple_spinner_dropdown_item));
        bottlespinner.setAdapter(adapter);


        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged (SeekBar seekBar, int progress, boolean fromUser) {
                amount = seekBar.getProgress() + 1;
                textMoney.setText(amount +" €");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    public void buyBottle(View v){
        BottleDispenser bd = BottleDispenser.getInstance();
        String c;
        System.out.println(index);
        c  = bd.buyBottle(index);
        text.setText(c);
    }

    public void addMoney(View v){
        int amount;
        BottleDispenser bd = BottleDispenser.getInstance();

        amount = seekBar.getProgress()+1;
        bd.addMoney(amount);
        text.setText(amount + "€ added");
        seekBar.setProgress(0);
    }

    public void returnMoney(View v){
        BottleDispenser bd = BottleDispenser.getInstance();
        String c;
        c = bd.returnMoney();
        text.setText(c);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        index = parent.getSelectedItemPosition();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
